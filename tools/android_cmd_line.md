# Android Command Line

Ensemble d'outils en ligne de commandes disponible sur Windows, MacOS et Linux. Pour cette page, je vais travailler uniquement sous Linux et utiliser le dossier `/opt/android` comme référence.

Les outils que l'on abordera sont :
- `sdkmanager`
- `adb`
- `emulator`

Il vous faudra avoir installer un JDK (ici 8) et avoir défini la variable `JAVA_HOME`. Si vous faites une installation de Java, il est intéressant de modifier le `PATH` pour lui ajouter `$JAVA_HOME/bin`.

## Installation de l'environnement

Pour installer ces outils, il faut télécharger une archive provenant du [site officiel](https://developer.android.com/studio#command-tools). Et la décompresser dans le dossier souhaité (pour moi : `/opt/android`)

On doit ensuite définir quelques variables d'environnement. Le détail de leur fonctionnement est disponible ici : https://developer.android.com/studio/command-line/variables.

| Variable d'environnement | Valeur|
| --- | --- |
| `ANDROID_SDK_ROOT` | `/opt/android` |

On va également ajouter `$ANDROID_SDK_ROOT` au `PATH`.

## SDK Manager

Cet outil est le premier que vous allez utiliser. Il permet d'installer les autres outils, SDK et NDK nécessaires.

Pour lister les paquets disponibles on utilise l'argument `--list`.

```bash
sdkmanager --sdk_root=$ANDROID_SDK_ROOT --list
```

On va commencer par installer adb, il se trouve dans les `platform-tools`.

```bash
yes | sdkmanager --sdk_root=$ANDROID_SDK_ROOT --install platform-tools
```

On ajoute maintenant les outils d'émulation.

```bash
yes | sdkmanager --sdk_root=$ANDROID_SDK_ROOT --install emulator
```

Ces outils doivent être ajouté au `PATH` pour simplifier leur utilisation. Ils se trouvent respectivement dans les dossiers `$ANDROID_SDK_ROOT/platform-tools` et ``

Sources : https://developer.android.com/studio/command-line/sdkmanager

## Création d'un émulateur Android

On installe en plus des éléments précédents la version d'Android que l'on veut utiliser sur l'émulateur (ici API 22 : Android 5.1).

```bash
sdkmanager --sdk_root=$ANDROID_SDK_ROOT "system-images;android-22;default;x86"
```

On accepte les licences d'utilisation.

```bash
yes Y | /opt/android/tools/bin/sdkmanager --licenses
```

On peut enfin créer l'émulateur.
```bash
echo "no" | avdmanager --verbose create avd --force --name "test" --device "pixel" --package "system-images;android-22;default;x86" --abi "x86"
```

Et on peut vérifier que tout à fonctionner.

```bash
emulator -list-avds
```

{% hint style="info" %}
J'ai rencontré un problème sur la configuration. Il faut donc corriger une configuration pour que l'émulateur fonctionne.

Dans le fichier `$HOME/.android/avd/<nom emulateur>/config.ini`, on modifie la ligne :

```
image.sysdir.1=android/system-images/android-22/default/x86/
```

par :


```
image.sysdir.1=system-images/android-22/default/x86/
```
{% endhint %}

On peut maintenant lancer l'émulateur en spécifiant que lon ne veut pas utiliser d'audio ni d'animation de démarrage. On le fait sans affichage graphique et sans accélération GPU. On active cependant l'accélération matérielle et on utilise une résolution de 1080x1920.

```bash
emulator -avd test -no-audio -no-boot-anim -no-window -accel on -gpu off -skin 1080x1920 &
```

L'émulateur se lance et on peut vérifier qu'il est bel et bien connecté.

```bash
adb devices
```

Pour arrêter l'émulateur on utilisera adb.

```bash
adb -s emulator-5554 emu kill
```

On peut ajouter une carte mémoire à l'appareil grâce à l'outil `mksdcard`.

```bash
mksdcard -l testSdCard 1024M testSdCard.img
```

Et on l'utilise au lancement de l'émulateur.

```bash
emulator -avd test -no-audio -no-boot-anim -no-window -accel on -gpu off -skin 1080x1920 -sdcard testSdCard.img &
```
